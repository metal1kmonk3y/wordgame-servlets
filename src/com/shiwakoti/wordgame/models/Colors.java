package com.shiwakoti.wordgame.models;

public class Colors {		
	
	private String guessBackground;
	private String wordBackground;
	private String textColor;
	
	public Colors(){
		
	}
	
	
	public Colors(String guessBackground, String wordBackground, String textColor) {
		this.guessBackground = guessBackground;
		this.wordBackground = wordBackground;
		this.textColor = textColor;
	}

	public String getGuessBackground() {
		return guessBackground;
	}

	public void setGuessBackground(String guessBackground) {
		this.guessBackground = guessBackground;
	}

	public String getWordBackground() {
		return wordBackground;
	}

	public void setWordBackground(String wordBackground) {
		this.wordBackground = wordBackground;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
}
