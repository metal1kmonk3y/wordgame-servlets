package com.shiwakoti.wordgame.models;

import java.util.List;

public class Metadata {
	
	private List<Font> fonts;
	private List<Level> levels;
	private Defaults defaults;
	
	public Metadata(){
		
	}
	
	
	public Metadata(List<Font> fonts, List<Level> levels, Defaults defaults) {
		this.fonts = fonts;
		this.levels = levels;
		this.defaults = defaults;
	}	
	


	public List<Font> getFonts() {
		return fonts;
	}
	
	public void setFonts(List<Font> fonts) {
		this.fonts = fonts;
	}

	public List<Level> getLevels() {
		return levels;
	}
	
	public void setLevels(List<Level> levels) {
		this.levels = levels;
	}

	public Defaults getDefaults() {
		return defaults;
	}
	
	public void setDefaults(Defaults defaults) {
		this.defaults = defaults;
	}	
}
