package com.shiwakoti.wordgame.models;

public class Font {	
	
	private String category;
	private String family;
	private String rule;
	private String url;
	
	public Font(){
		
	}
	
	public Font(String category, String family, String rule, String url) {		
		this.category = category;
		this.family = family;
		this.rule = rule;
		this.url = url;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}	
	
}
