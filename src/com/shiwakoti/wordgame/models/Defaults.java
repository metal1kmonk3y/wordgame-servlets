package com.shiwakoti.wordgame.models;

public class Defaults {
	
	private Font font;
	private Level level;
	private Colors colors;
	
	public Defaults() {	
	}
	
	public Defaults(Font font, Level level, Colors colors) {
		this.font = font;
		this.level = level;
		this.colors = colors;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public Colors getColors() {
		return colors;
	}

	public void setColors(Colors colors) {
		this.colors = colors;
	}	
}
