package com.shiwakoti.wordgame.models;

public class Game {	

	private Colors colors;
	private Font font;
	private String guesses;
	private String id;
	private Level level;
	private int remaining;
	private String status;
	private String target;
	private long timestamp;
	private long timeToComplete;
	private String view;	
	
	private Game(Colors colors, Font font, String guesses, String id, Level level, int remaining, String status,
			String target, long timestamp, long timeToComplete, String view) {
		this.colors = colors;
		this.font = font;
		this.guesses = guesses;
		this.id = id;
		this.level = level;
		this.remaining = remaining;
		this.status = status;
		this.target = target;
		this.timestamp = timestamp;
		this.timeToComplete = timeToComplete;
		this.view = view;
	}
	
	public static class Builder{
		private Colors colors;
		private Font font;
		private String guesses;
		private String id;
		private Level level;
		private int remaining;
		private String status;
		private String target;
		private long timestamp;
		private long timeToComplete;
		private String view;
		
		public Game build(){
			return new Game(this.colors, this.font, this.guesses,
						this.id, this.level, this.remaining, this.status,
						this.target, this.timestamp, this.timeToComplete, this.view);
		}
		
		public Builder colors(Colors colors){
			this.colors = colors;
			return this;			
		}
		
		public Builder font(Font font){
			this.font = font;
			return this;			
		}
		
		public Builder guesses(String guesses){
			this.guesses = guesses;
			return this;			
		}
		
		public Builder id(String id){
			this.id = id;
			return this;			
		}
		
		public Builder level(Level level){
			this.level = level;
			return this;			
		}
		
		public Builder remaining(int remaining){
			this.remaining = remaining;
			return this;			
		}		
		
		public Builder status(String status){
			this.status = status;
			return this;			
		}
		
		public Builder target(String target){
			this.target = target;
			return this;			
		}
		
		public Builder timestamp(long timestamp){
			this.timestamp = timestamp;
			return this;			
		}
		
		public Builder timeToComplete(long timeToComplete){
			this.timeToComplete = timeToComplete;
			return this;			
		}
		
		public Builder view(String view){
			this.view = view;
			return this;			
		}		
	}
	
	
	public Colors getColors() {
		return colors;
	}

	public void setColors(Colors colors) {
		this.colors = colors;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public String getGuesses() {
		return guesses;
	}

	public void setGuesses(String guesses) {
		this.guesses = guesses;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public int getRemaining() {
		return remaining;
	}

	public void setRemaining(int remaining) {
		this.remaining = remaining;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimeToComplete() {
		return timeToComplete;
	}

	public void setTimeToComplete(long timeToComplete) {
		this.timeToComplete = timeToComplete;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}
	
	
	
}
