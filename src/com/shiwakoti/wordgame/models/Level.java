package com.shiwakoti.wordgame.models;

public class Level {	
	
	private int guesses;
	private int maxLength;
	private int minLength;
	private String name;
	
	public Level(){
		
	}	
	
	public Level(int guesses, int maxLength, int minLength, String name) {		
		this.guesses = guesses;
		this.maxLength = maxLength;
		this.minLength = minLength;
		this.name = name;
	}

	public int getGuesses() {
		return guesses;
	}

	public void setGuesses(int guesses) {
		this.guesses = guesses;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
