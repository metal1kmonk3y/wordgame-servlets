package com.shiwakoti.wordgame;


import java.io.IOException;
import java.util.Date;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.shiwakoti.wordgame.models.Colors;
import com.shiwakoti.wordgame.models.Font;
import com.shiwakoti.wordgame.models.Game;
import com.shiwakoti.wordgame.models.Level;
import com.shiwakoti.wordgame.services.DictionaryService;
import com.shiwakoti.wordgame.services.GameService;
import com.shiwakoti.wordgame.services.MetadataService;

/**
 * Servlet implementation class GamesServlet
 */
@WebServlet("/api/v1/games/*")
public class GamesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private MetadataService metadataService;  
	private DictionaryService dictionaryService;
	private GameService gameService;
    
	public void init(){
		try {
			metadataService = MetadataService.getInstance();
			dictionaryService = DictionaryService.getInstance();
			gameService = GameService.getInstance();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {				
		response.getWriter().println(Utilities.toJSON(gameService.findAllSessionGames()));		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {						
		String levelName = request.getParameter("level");
		String fontFamily = request.getHeader("X-font");
		
		Colors colors = new Colors();	
		colors.setGuessBackground(request.getParameter("guessBackground")); 
		colors.setWordBackground(request.getParameter("wordBackground"));   
		colors.setTextColor(request.getParameter("textColor")); 		
						
		Level level = metadataService.getLevels().get(0);
		Font font =  metadataService.getFonts().get(0);
		
		for(Font f : metadataService.getFonts()){
			if(f.getFamily().equals(fontFamily)){
				font = f;
			}
		}		   
		
		for(Level l : metadataService.getLevels()){
			if(l.getName().equals(levelName)){
				level = l;
				break;
			}			 
		}
		
		String target = dictionaryService.getWordfor(level);
		String view = "";
		
		for(int i = 0; i < target.length(); i++){
			view += "_";
		}

		Game game = new Game.Builder()
						.colors(colors)
						.font(font)
						.guesses("")
						.id(Utilities.getUid())
						.level(level)
						.remaining( level.getGuesses())
						.status("unfinished")
						.target("")
						.timestamp(new Date().getTime())
						.timeToComplete(0)
						.view(view)
						.build();			
		
		String gameString = Utilities.toJSON(game);
		 
		game.setTarget(target);
		gameService.submit(game);
		
		response.getWriter().append(gameString);		
	}
}
