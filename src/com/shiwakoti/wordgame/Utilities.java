package com.shiwakoti.wordgame;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class Utilities {
	
	public static String toJSON( Object o) throws JsonGenerationException, JsonMappingException, IOException{
		
		return new ObjectMapper().writeValueAsString(o);		
	}
	
	public static <E> E fromJSON(File f, Class<E> clzz ) throws JsonParseException, JsonMappingException, IOException{		
		return new ObjectMapper().readValue(f, clzz);		
	}
	
	public static <E> E fromJSON(String s, Class<E> clzz ) throws JsonParseException, JsonMappingException, IOException{		
		return new ObjectMapper().readValue(s, clzz);		
	}
	
	public static String splitPathAt(HttpServletRequest request, int index){
		return request.getRequestURI().split("/")[index];
	}
	
	public static String getUid(){	
		int length = 64;
		String uid = "";		
		String possibleChars  = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";		
		
		for(int i = 0; i < length; i ++){			
			int r =  (int) (Math.random() * possibleChars.length());			
			uid += possibleChars.charAt(r);
		}		
		
		return uid;
	}
}
