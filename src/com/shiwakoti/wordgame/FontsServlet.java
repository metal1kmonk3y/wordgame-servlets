package com.shiwakoti.wordgame;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shiwakoti.wordgame.services.MetadataService;

@WebServlet("/api/v1/meta/fonts")
public class FontsServlet extends HttpServlet {
	private static final long serialVersionUID = 12L;
	private MetadataService metadataService;   
    
	public void init(){
		try {
			metadataService = MetadataService.getInstance();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().println(Utilities.toJSON(metadataService.getFonts()));
	}	
}
