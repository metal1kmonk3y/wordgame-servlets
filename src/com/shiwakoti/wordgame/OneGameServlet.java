package com.shiwakoti.wordgame;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shiwakoti.wordgame.models.Game;
import com.shiwakoti.wordgame.services.GameService;

/**
 * Servlet implementation class OneGameServlet
 */
@WebServlet("/api/v1/game/*")
public class OneGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private GameService gameService;
    
	public void init(){			
			gameService = GameService.getInstance();	
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String gid = Utilities.splitPathAt(request, 6);		
		Game game = gameService.find(gid);
		String target = game.getTarget();
		
		String gameString = Utilities.toJSON(game);
		
		 if(game.getStatus().equals("unfinished")){
			game.setTarget(""); 
			gameString = Utilities.toJSON(game);
			game.setTarget(target);
		 }
		
		response.getWriter().println(Utilities.toJSON(gameString));
	}
}
