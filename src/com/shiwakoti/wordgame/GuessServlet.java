package com.shiwakoti.wordgame;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shiwakoti.wordgame.models.Game;
import com.shiwakoti.wordgame.services.GameService;

/**
 * Servlet implementation class GuessServlet
 */
@WebServlet("/api/v1/guesses/*")
public class GuessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   	
	private GameService gameService;
    
	public void init(){			
			gameService = GameService.getInstance();	
	}	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String gid = Utilities.splitPathAt(request, 6);	
		char guess = request.getParameter("guess").charAt(0);
		Boolean gotAtLeastOne = false;
		Game game = gameService.find(gid);
		String target = game.getTarget();
		
		game.setGuesses(game.getGuesses() + guess);
		
		 for(int i = 0; i < target.length(); i++){
            if(target.charAt(i) == guess){

                String temp = game.getView();
                game.setView(temp.substring(0, i) + guess +  temp.substring(i + 1));
                gotAtLeastOne = true;
            }
        }
		 
		 if(target.equals(game.getView())){
			 game.setStatus("won");
			 game.setTimeToComplete(new Date().getTime() - game.getTimestamp());
		 } else if (!gotAtLeastOne){
			 game.setRemaining(game.getRemaining() - 1);
			 
			 if(game.getRemaining() == 0){
				 game.setStatus("lost");
				 game.setTimeToComplete(new Date().getTime() - game.getTimestamp());
			 }
		 }
		 
		 String gameString = Utilities.toJSON(game);
		 
		 if(game.getStatus().equals("unfinished")){
			game.setTarget(""); 
			gameString = Utilities.toJSON(game);
			game.setTarget(target);
		 }
		 
		 response.getWriter().println(gameString);		 
	}
}
