package com.shiwakoti.wordgame;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/api/v1/sid")
public class SessionServlet extends HttpServlet {  
	private static final long serialVersionUID = 6264615722534879978L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
				
		response.getWriter().println(Utilities.toJSON(Utilities.getUid()));
	}	
	
	
}


