package com.shiwakoti.wordgame.services;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.shiwakoti.wordgame.Utilities;
import com.shiwakoti.wordgame.models.Font;
import com.shiwakoti.wordgame.models.Level;
import com.shiwakoti.wordgame.models.Metadata;

public class MetadataService {
		
	private Metadata meta;	
	
	private static MetadataService instance = null;
	
	private MetadataService() throws JsonParseException, JsonMappingException, IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("/com/shiwakoti/wordgame/repo/metadata.json").getFile());
		setMeta(Utilities.fromJSON( file, Metadata.class));
	}
	
	public static MetadataService getInstance() throws JsonParseException, JsonMappingException, IOException{
		if(instance == null){
			instance = new MetadataService();
		}
		return instance;
	}

	public Metadata getMeta() {
		return meta;
	}

	public void setMeta(Metadata meta) {
		this.meta = meta;
	}
	
	public List<Font> getFonts(){
		return meta.getFonts();
	}
	
	public List<Level> getLevels(){
		return meta.getLevels();
	}
}
