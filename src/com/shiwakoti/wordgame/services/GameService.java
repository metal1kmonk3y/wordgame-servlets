package com.shiwakoti.wordgame.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.shiwakoti.wordgame.models.Game;

public class GameService {
	private Map<String, Game> allGames = new HashMap<>();
	private ArrayList<Game> allSessionGames = new ArrayList<>();
	
	public static GameService instance = null;
	
	private  GameService(){
		
	}
	
	public static GameService getInstance(){
		if( instance == null ) {
			instance = new GameService();
		}
		return instance;
	}
	
	public void submit (Game g){
		allGames.put(g.getId(), g);	
		allSessionGames.add(g);
	}
	
	public Game find (String gid){
		return allGames.get(gid);
	}	
	
	public ArrayList<Game> findAllSessionGames (){
		return allSessionGames;
	}	
}
