package com.shiwakoti.wordgame.services;

import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.shiwakoti.wordgame.models.Level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DictionaryService {
	
	private List<String> wordList = new ArrayList<>();
	
	private static DictionaryService instance = null;
	
	private DictionaryService() throws JsonParseException, JsonMappingException, IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("/com/shiwakoti/wordgame/repo/wordlist.txt").getFile());
		
		FileReader fileIn = new FileReader(file);
		BufferedReader read = new BufferedReader(fileIn);
		String line = read.readLine();
		
		while(line != null){
			
			wordList.add(line);
			line = read.readLine();
		}
		
		read.close();		
	}
	
	public static DictionaryService getInstance() throws JsonParseException, JsonMappingException, IOException {
		if(instance == null){
			instance = new DictionaryService();
		}
		return instance;
	}

	public List<String> getWordList() {
		return wordList;
	}

	public void setWordList(List<String> wordList) {
		this.wordList = wordList;
	}	
	
	public String getWordfor(Level level){		
		int index = (int) (Math.random() * wordList.size());
		String word = wordList.get(index);
		int wordLength = word.length();
		
		if(wordLength > level.getMinLength() && wordLength < level.getMaxLength() ){
			return word;
		}
		
		return getWordfor(level);		
	}
}
