/*
* CS402-HW5
*
* @Author Prasanna Shiwakoti
* @Modified 4/14/2017
*/

var font, level, colors, allFonts, allLevels,  game;
var hasSid = false;

/* getting a a particular cookie by traversing all cookies  */
var getThisCookie = function (name) {

    var allCookies = document.cookie;
    var cookieArray = allCookies.split(";");

    for(var i = 0; i < cookieArray.length; i++){
        var key = cookieArray[i].split("=")[0];
        var value = cookieArray[i].split("=")[1];

        if(key && value && key == name){
            return value;
        }
    }
};

/* setting a a particular cookie */
var  setThisCookie = function(key, value) {

    document.cookie = key + value;
};

/* sets Sid */
var setSid = function (data) {

    if(!getThisCookie("X-sid")) {
        setThisCookie("X-sid=", JSON.parse(data));
        
    }
    hasSid = true;
}

/* starts new game by changing view, and fetching word */
var newGame = function(evt){

    evt.preventDefault();

    $.ajax({
        url: "/wordgame/api/v1/games/" + getThisCookie("X-sid") + "?level=" + level.name,
        headers: {"X-font": "" + fontFamily},
        method: "POST",
        data: colors,
        success: init
    });
};

/* initialize variables and show game view */
var init = function (data) {

    $("#game-list").slideUp();
    $("#game").show();
    $(".guess-form").show();   

    updateGame(data);
};

/* updating game after a guess */
var updateGame = function (data) {


    game = JSON.parse(data);
    
    $("#target-word").children().remove();

    setUp();

    /* if this is true player wins */
    if(game.status == "won"){
        $(".guess-form").hide();
        $("#game-well").css("background-image", "url(/wordgame/images/winner.gif)");
    }
    /* if this is true player loses */
    else if(game.status == "lost" ){
        $(".guess-form").hide();
        $("#game-well").css("background-image", "url(/wordgame/images/loser.gif)");
    }
};

/* set up game */
var setUp = function () {

    $(".guess-count").text(game.remaining + " guesses remaining.");   
    
    for(var i = 0; i < game.view.length; i++){
        $("#target-word").append("<span class=\"hidden-letter\">" + game.view.charAt(i).toUpperCase()  + "</span>");
    }   
   
    $("#guess-word").children().remove();
    for(var i = 0; i < game.guesses.length; i++){
   	 $("#guess-word").append("<span class=\"guessed-letter\">" + game.guesses.charAt(i).toUpperCase() +  "</span>");
   }

    $("#game-words").css({"color" : game.colors.textColor});
    $("#target-word").children().css({"background-color" : game.colors.wordBackground});
};

/* checks if guessed letter is part of the word */
var hangman = function (evt) {

    evt.preventDefault();

    var guessedLetter = $("#guess-input").val();
    if(guessedLetter == ""){
        return;
    }

    $("#guess-input").val("");

    /* check to see if letter was already guessed */
    if(game.guesses.includes(guessedLetter)){
        return;
    }

    /* display new letter guess*/
    $("#guess-word").append("<span class=\"guessed-letter\">" + guessedLetter.toUpperCase() +  "</span>");
    $("#guess-word").children().css({"background-color" : game.colors.guessBackground});

    $.ajax({
        url: "/wordgame/api/v1/guesses/" + getThisCookie("X-sid") + "/" + game.id + "/guesses?guess=" + guessedLetter,
        method: "POST",
        success: updateGame
    });
};

/* ends game by returning to game-list view */
var endGame = function(){

    $("span").remove(".guessed-letter, .hidden-letter");
    $("#game-well").css("background-image","none");
    $("#game").hide();
    $("#game-list").slideDown();
    
    game = null;
    getGameList();
};

/* getting game list */
var getGameList = function(){

    $.ajax({
        url: "/wordgame/api/v1/games/" + getThisCookie("X-sid"),
        method: "GET",
        success: setTable
    });
}

/* setting up table that displays all game for current session */
var setTable = function (data) {

    var sessionGames = JSON.parse(data);

    $("#current-games-list").children().remove();

    for (var  i = 0; i <  sessionGames.length; i++){

        var answer = "";
        var tableLeft = "<tr onclick=\"retryGame(id)\" id=\"" + sessionGames[i].id + "\""+ ">";
        var tableRight = "</tr>";

        if(sessionGames[i].status != "unfinished"){
            answer = sessionGames[i].target;
        }

        var unfinishedGuess= "";
        for(var j = 0; j < sessionGames[i].view.length; j++){
            unfinishedGuess = unfinishedGuess + "<span class=\"guessed-letter-sm mystery-letter_"  + i + "\">" + sessionGames[i].view.charAt(j).toUpperCase()  + "</span>";
        }

        $("#current-games-list").append($(tableLeft + "<td>" +
                sessionGames[i].level.name + "</td><td>" +
                unfinishedGuess + "</td><td>" +
                sessionGames[i].remaining +  "</td><td>" +
                answer +  "</td><td>" +
                sessionGames[i].status +
            "</td>" + tableRight
        ));

        $(".mystery-letter_" + i).css("font-family", sessionGames[i].font.family);
        $(".mystery-letter_" + i).css("color", sessionGames[i].colors.textColor);
        $(".mystery-letter_" + i).css("background-color", sessionGames[i].colors.wordBackground);
    }
};

/* retrying an unfinished game */
var retryGame = function (id) {
	
    $.ajax({
        url: "/wordgame/api/v1/game/" + getThisCookie("X-sid") + "/" + id,
        method: "GET",
        success: init
    });
};

/* getting game metadata */
var getMetaData = function () {

    $.ajax({
        url: "/wordgame/api/v1/meta",
        method: "GET",
        success: setOptions
    });
};

/* setting up game metadata */
var setOptions = function (data) {
	
	var options = JSON.parse(data);
	options = options.meta;
	
    font = options.defaults.font;
    fontFamily = font.family;
    level = options.defaults.level;
    colors = options.defaults.colors;
    allFonts = options.fonts;
    allLevels = options.levels;
   

    /* setting all colors */
    $("#word-color").val(colors.wordBackground);
    $("#guess-color").val(colors.guessBackground);
    $("#fore-color").val(colors.textColor);

    /* adding all levels */
    allLevels.forEach(function (level) {
        $("#level-picker").append($("<option></option>").text(level.name));
    });

    /* setting level to default */
    $("#level-picker").val(level.name);

    /* adding all fonts */
    allFonts.forEach(function (font) {
        $("meta").after($("<link rel=\"stylesheet\" type=\"text/css\"/>").attr("href", font.url));
        $("#font-picker").append($("<option></option>").text(font.family));
    });

    /* setting font to default */
    $("#font-picker , #game-words").css({"font-family" : fontFamily});
    $("#font-picker").val(fontFamily);
};

/* getting a a particular cookie by traversing all cookies  */
var main = function() {

    /* checking for session id */
    if(!hasSid){
        $.ajax({
            url:"/wordgame/api/v1/sid",
            method: "GET",
            success: setSid
        });
    }

    /* getting game metadata */
    getMetaData();

    /* hide game view on page load */
    $("#game").hide();

    /* changing font to selected one */
    $("#font-picker").change(function(){
        fontFamily = $("#font-picker option:selected").text();
        $("#font-picker , #game-words").css({"font-family" : fontFamily});
    });

    /* changing level to selected one */
    $("#level-picker").change(function(){  
    	var levelName = $("#level-picker option:selected").text();
    	
        allLevels.forEach(function (l) {
            if(l.name = levelName){
            	level = l;
            }
        });
        
    });

    /* changing color to selected one */
    $("#word-color").change(function(){
        colors.wordBackground = $("#word-color").val();
    });

    $("#guess-color").change(function(){
        colors.guessBackground = $("#guess-color").val();
    });

    $("#fore-color").change(function(){
        colors.textColor = $("#fore-color").val();
    });
};

$(document).ready(main);